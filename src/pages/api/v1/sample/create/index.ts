import { NextApiRequest, NextApiResponse } from "next";

import prisma from "@/lib/prisma/prisma"

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
    const { username, password, fullname } = await req.body;
    const data: any = { username, password, fullname };

    if (req.method === 'POST') {
        const createUser = await prisma.user.create({ data });
        if (!createUser) return res.status(500).json({ code: 500, message: "INTERNAL_SERVER_ERROR" });

        return res.status(200).json({ code: 201, message: "CREATED" })
    } else {
        return res.status(405).json({ code: 405, message: "Unsupported HTTP Method" });
    }

}