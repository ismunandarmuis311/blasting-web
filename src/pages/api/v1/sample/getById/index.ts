

import { NextApiRequest, NextApiResponse } from "next";

import prisma from "@/lib/prisma/prisma";

export default async function handler(req: NextApiRequest, res: NextApiResponse) {

    if (req.method === 'GET') {
        const id = req.query.id;
        const getById: any = await prisma.user.findFirst({
            where: {
                id: Number(id)
            }
        })

        if (!getById) {
            return res.status(200).json({ code: 200, message: "OK", data: {} });
        } else {
            return res.status(200).json({ code: 200, message: "OK", data: getById });

        }
    } else {
        return res.status(405).json({ code: 405, message: "Unsupported HTTP Method" });
    }

    // if (req.method === 'GET') {
    //     if (data.length < 1) return res.status(200).json({ code: 200, message: "OK", data: [] });

    //     return res.status(200).json({ code: 200, message: "OK", data: data });
    // } else {
    //     return res.status(405).json({ code: 405, message: "Unsupported HTTP Method" });
    // }
}