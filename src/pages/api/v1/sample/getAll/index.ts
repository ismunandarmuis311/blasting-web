import { NextApiRequest, NextApiResponse } from "next";
import prisma from "@/lib/prisma/prisma";

export default async function handler(req: NextApiRequest, res: NextApiResponse) {

    if (req.method === 'GET') {
        const getAll = await prisma.user.findMany();
        if (getAll.length < 1) {
            return res.status(200).json({ code: 200, message: "OK", data: [] });
        }
        return res.status(200).json({ code: 200, message: "OK", data: getAll });
    } else {
        return res.status(405).json({ code: 405, message: "Unsupported HTTP Method" });
    }
}