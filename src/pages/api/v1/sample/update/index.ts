import { NextApiRequest, NextApiResponse } from "next";

import prisma from "@/lib/prisma/prisma";

export default async function handler(req: NextApiRequest, res: NextApiResponse) {

    if (req.method == 'PUT') {
        const id = req.query.id;
        const data = req.body;

        const updateData = await prisma.user.update({
            where: {
                id: Number(id)
            },
            data: data
        })

        if (!updateData) {
            return res.status(400).json({ code: 400, message: "BAD_REQUEST" });
        } else {
            return res.status(200).json({ code: 200, message: "OK", data: data });

        }
    } else {
        return res.status(405).json({ code: 405, message: "Unsupported HTTP Method" });
    }
}