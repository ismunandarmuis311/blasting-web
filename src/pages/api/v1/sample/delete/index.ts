import { NextApiRequest, NextApiResponse } from "next";

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
    if (req.method === 'DELETE') {
        const { id } = req.body;

        const deleteData = await prisma.user.delete({
            where: {
                id: Number(id)
            }
        });

        console.log(deleteData);

        if (!deleteData) {
            return res.status(400).json({ code: 400, message: "BAD_REQUEST" });
        } else {
            return res.status(200).json({ code: 200, message: "OK" });
        }
    } else {
        return res.status(405).json({ code: 405, message: "Unsupported HTTP Method" });
    }
}